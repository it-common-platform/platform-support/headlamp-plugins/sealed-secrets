# sealed-secrets

This Headlamp plugin adds a `Sealed Secrets` link in the sidebar that linkes to a GUI to create sealed secrets.

## Developing Headlamp plugins

For more information on developing Headlamp plugins, please refer to:

- [Platform Onboarding](https://onboarding.platform.it.vt.edu/development-environment/headlamp-plugins/)
- [Getting Started](https://headlamp.dev/docs/latest/development/plugins/), How to create a new Headlamp plugin.
- [API Reference](https://headlamp.dev/docs/latest/development/api/), API documentation for what you can do
- [UI Component Storybook](https://headlamp.dev/docs/latest/development/frontend/#storybook), pre-existing components you can use when creating your plugin.
- [Plugin Examples](https://github.com/headlamp-k8s/headlamp/tree/main/plugins/examples), Example plugins you can look at to see how it's done.
